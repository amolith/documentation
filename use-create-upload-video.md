# Create a channel and upload a video

## Create a new channel

To create a new channel, you have to:

  1. click <i data-feather="more-vertical"></i> next to your username
  1. click **Channel settings**
  1. click **Create video channel** button
  1. enter informations:
    1. **Name**: your channel's name (e.g.: `cats@my-insance.tld`)
    1. **Display Name**: can be different from **Name**
    1. **Description**: what's you channel is about? (supports markdown)
    1. **Support**: where you can link to financing platforms or directly materials to support your channel
  1. click **Create** button

Once your channel created, you can change its avatar by:

  1. clicking **Update** button in front of its name in your channels list
  1. clicking <i data-feather="edit-2"></i> icon on the default avatar
  1. selecting an image on your computer
  1. clicking **Update**


Your channel is all set to have some videos!

## Upload a video

To uplaod a video you have to click the <i data-feather="upload-cloud"></i>**Upload** button in top right corner. Once clicked you have 3 ways to upload a video:

  1. [by selecting a file on your device](use-create-upload-video?id=upload-a-file);
  1. [by importing an online video by its url](use-create-upload-video?id=import-with-url);
  1. [by importing an online video by its URI (torrent)](use-create-upload-video?id=import-with-torrent).

### Upload a file

Once you have to click the <i data-feather="upload-cloud"></i>**Upload** button in top right corner:

  1. select which channel you want to upload your video (can be done/change after upload);
  1. select privacy settings for this video (can be done/change after upload);
  1. click **Select file to upload** button.

While the video is uploading you can set [some details](use-create-upload-video?id=video-details).

### Import with URL

You can import any URL [supported by youtube-dl](https://rg3.github.io/youtube-dl/supportedsites.html) or URL that points to a raw MP4 file. To do so, you have to:

  1. click the <i data-feather="upload-cloud"></i>**Upload** button in top right corner;
  1. click **Import with URL** tab;
  1. paste your video's url into **URL** field;
  1. select which channel you want to upload your video (can be done/change after upload);
  1. select privacy settings for this video (can be done/change after upload);
  1. click **Import** button.

!> You should make sure you have diffusion rights over the content it points to, otherwise it could cause legal trouble to yourself and your instance.

While the video is uploading you can set [some details](use-create-upload-video?id=video-details).

### Import with torrent

You can import any torrent file that points to a mp4 file. To do so, you have to:

  1. click the <i data-feather="upload-cloud"></i>**Upload** button in top right corner;
  1. click **Import with torrent** tab;
  1. select a `.torrents` file on your commputer or paste magnet URI of a video;
  1. select which channel you want to upload your video (can be done/change after upload);
  1. select privacy settings for this video (can be done/change after upload);
  1. click **Import** button.

!> You should make sure you have diffusion rights over the content it points to, otherwise it could cause legal trouble to yourself and your instance.

While the video is uploading you can set [some details](use-create-upload-video?id=video-details).

### Video details

#### Basic info

  * **Title**: the name of the video (something more catchy than `myvideo.mp4` for example :wink: );
  * **Tags**: tags can be used to suggest revelant recommandations. 5 tags maximum. You have to press enter to add one;
  * **Description**: text you want to display above your video (supports markdown) - you can see how it looks above;
  * **Channel**: in which channel you want to add your video;
  * **Category**: what kind of content is your video (Activism? How to? Music?);
  * **Licence**:
    * Attribution,
    * Attribution - Share Alike,
    * Attribution - No Derivatives,
    * Attribution - Non Commercial,
    * Attribution - Non Commercial - Share Alike,
    * Attribution - Non Commercial - No Derivatives,
    * Public Domain Dedication;
  * **Language**: what is the main language in the video;
  * **Privacy**: public, internal, unlisted, private. [See what it means](use-create-upload-video?id=video-confidentiality-options-what-do-they-mean);
  * **Contains sensitive content**? Some instances do not list videos containing mature or explicit content by default.

![user upload basi info - image](/assets/user-upload-video-basic-info.png)

#### Captions

This tab allows you to add subtitles to your video. To add one, you have to:

  1. go to **Captions** tab;
  1. click <i data-feather="plus-circle"></i> **Add another caption** button;
  1. select the caption language in the list;
  1. click **Select the caption file** button;
  1. browse into your computer file to select your `.vtt` or `.srt` file;
  1. click **Add this caption** button;
  1. click **<i data-feather="check-circle"></i> Update** button.

Your caption is now available by clicking <i data-feather="settings"></i> > **Subtitles/CC** and selecting the language.

To delete a caption you have to:

  1. click **Delete** button in front of the language you want to delete;
  1. click **<i data-feather="check-circle"></i> Update** button.

#### Advanced settings

This tab allows you:

  * to edit your preview image;
  * to add a short text to tell people how they can support you (membership platform...);
  * change the original publication date;
  * disable/enable video comments;
  * disable/enable download of your video.

?> Click **<i data-feather="check-circle"></i> Update** button to save your new settings.

## Video confidentiality options: what do they mean?

  * **Public**: your video is public. Everyone can see it (by using search engine, link or embed);
  * **Internal**: only authenticated user having an account on your instance can see your video. Users searching from another instance or having the link without being authentifaced can't see it;
  * **Unlisted**: only people with the private link can see this video; the video is not visible without its link (can't be found by searching);
  * **Private**: only you can see the video.
